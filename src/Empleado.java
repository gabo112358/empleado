
public class Empleado {

    //Definicion de atributos de la clase
    private String nombre;
    private String identificacion;
    private char genero;
    private int edad;
    private String puesto;

    //Constructor por defecto
    public Empleado() {
        this.nombre = "";
        this.identificacion = "";
        this.genero = 'O';
        this.edad = 0;
        this.puesto = "";
    }

    //Constructor con parámetros
    public Empleado(String nombre, String identificacion, char genero, int edad, String puesto) {
        this.nombre = nombre;
        this.identificacion = identificacion;
        this.genero = genero;
        this.edad = edad;
        this.puesto = puesto;

    }

    //metodos set get
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public char getGenero() {
        return genero;
    }

    public void setGenero(char genero) {
        this.genero = genero;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
    //metodo toString
    @Override
    public String toString() {
        return "Empleado" + ", nombre" + nombre + ", identificacion" + identificacion + ",genero" + genero + ", puesto" + puesto + ", edad" + edad;
    }

}
