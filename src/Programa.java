
import java.io.*;

public class Programa {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Empleado[] empleados = new Empleado[5];

    public static void main(String[] args) throws IOException {

        mostrarMenu();
    }

    public static void mostrarMenu() throws IOException {
        int opcion = 0;

        do {
            System.out.println("***Bienvenido al sistema***");
            System.out.println("1. Agregar una empleado.");
            System.out.println("2. Listar empleados.");
            System.out.println("3. Salir.");
            System.out.println("Dijite al opción que desea");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 3);

    }//fin class

    public static void procesarOpcion(int opcion) throws IOException {
        switch (opcion) {
            case 1:
                agregarEmpleado();
                break;
            case 2:
                listarEmpleados();
                break;
            case 3:
                System.out.println("Muchas gracias por su vicita");
                System.exit(0);
                break;
            default:
                System.out.println("opción inválida");
        }//fin switch
    }//fin class

    public static void agregarEmpleado() throws IOException {
        
        System.out.println("Ingrese el nombre del empleado");
        String nombre = in.readLine();       
        System.out.println("Ingrese la cédula del empleado");
        String identificacion = in.readLine();
        System.out.println("Ingrese el género del empleado (M-masculino, F-femenino):");
        char genero = in.readLine().charAt(0);        
        System.out.println("ingrese la edad delo empleado");
        int edad = Integer.parseInt(in.readLine());        
        System.out.println("Ingrese el puesto del empleado");
        String puesto = in.readLine();

        Empleado empleado = new Empleado(nombre, identificacion, genero, edad, puesto);

        for (int x = 0; x < empleados.length; x++) {
            if (empleados[x] == null) {
                empleados[x] = empleado;
                x = empleados.length;
            }
        }//fin for

    }//fin clase

    public static void listarEmpleados() {
        for (int x = 0; x < empleados.length; x++) {
            if (empleados[x] != null) {
                System.out.println(empleados[x].toString());
            }
        }//fin for
    }

}//fin programa
